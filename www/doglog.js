//var url="http://127.0.0.1\:3000"
var url="http://doglog-wlfrn.rhcloud.com"
//define app
angular.module('doglog',['ngResource']);

//define restful inteface with ngResource
// a la http://www.sitepoint.com/creating-crud-app-minutes-angulars-resource/
// creates get() query() save() remove() and delete()
angular.module('doglog').factory('Dog',
   function($resource) {
     return $resource(
       url+"/api/dog/:dog_id", 
       {dog_id: '@dog_id'},
       {
        update:{method:'PUT'}
       }
      );
   }
);

angular.module('doglog').factory('Log',
   function($resource) {
     return $resource(
       url+"/api/log/:dog_id/:log_id", 
       {dog_id: '@dog_id', log_id: '@log_id'},
       {
        update:{method:'PUT'}
       }
      );
   }
);

angular
 .module('doglog')
 .controller('LogCtrl',
   function($scope,$timeout,Dog,Log) {
     /*
     $scope.newp = new Dog();
     $scope.newp.data = {'dob': 1986, 'name': 'Will'}; 

     $scope.newp.saveval = Dog.save($scope.newp,function(){});

     $scope.pep1    = Dog.get({'dog_id': 1});
     $scope.people  = Dog.query();

     $scope.test="im in your page, eating your code";
     */

     //fake some data
     $scope.dog = { name: "Maslow", id: 1, dob: "2014/10/27"};
     $scope.logs=Log.query({'dog_id': $scope.dog.id});

     // the log we are going to add
     $scope.log = new Log({data: 
             {did: null,
              outside: 1,
              amount: 0,
              when: 0}
            } );

     console.log($scope.log);
     console.log('force save nonsense ');
     $scope.log.$save({dog_id:1});


     // do we stop the counter
     // -- stopped if we focus the any field for modification
     // counter starts when we select an option
     $scope.stpcnt = 0;
     $scope.timer  = 1; 
     // how much to decrease over what interval
     var cntdwn_by = 0.1;  
     var cntdwn_intv = 100; //ms
     // initilize time out value, modified recursively
     var mytimeout;

     // timer count down: recursive function to shrink 
     var timerfnc = function cd(){
       // recursively call timeout
       // until we are out of time or told to stop
       if($scope.timer<=0 || $scope.stpcnt ) {
          $timeout.cancel(mytimeout);
          $scope.timer=1;
          $scope.log = { data: { did: null, when: 0} };
          return;
       }

       // dec count by 
       $scope.timer-= cntdwn_by;

       if($scope.timer <= 0) {
         //TODO: submit form
         console.log("TODO: sumbit form!");
         console.log($scope.log);
         console.log("saving log!");
         $scope.log.$save({dog_id:1});

       }

       // count down again! (recurse)
       mytimeout = $timeout(timerfnc,100);
     };


     // on select
     $scope.startTimer = function() {
       if($scope.log.data.did === 'pee' || $scope.log.data.did === 'pop' ) {
         // outside, no amount tracked
         $scope.log.data.outside = 1;
         console.log('pee or poop!');
         $scope.log.data.amount= 0;
       } else {
         console.log('NOT pee or poop!');
         // not outside
         $scope.log.data.outside = 0;
         // units for eating/drinking
         if($scope.log.data.did==='eat') {
           $scope.log.data.amount= 0.5;
           $scope.am_units='cups';
         }else {
          $scope.log.data.amount= 200;
          $scope.am_units='ml';
         }
       }
       $timeout(timerfnc, cntdwn_intv );
     };

  });

angular.module('doglog').config(['$httpProvider',
    function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
]);

