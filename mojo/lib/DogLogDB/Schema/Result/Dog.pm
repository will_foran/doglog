use utf8;
package DogLogDB::Schema::Result::Dog;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

DogLogDB::Schema::Result::Dog

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<dog>

=cut

__PACKAGE__->table("dog");

=head1 ACCESSORS

=head2 dog_id

  data_type: 'integer auto_increment'
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 60

=head2 dob

  data_type: 'date'
  is_nullable: 1

=head2 dog_type

  data_type: 'varchar'
  is_nullable: 1
  size: 60

=head2 dog_pass

  data_type: 'varchar'
  is_nullable: 1
  size: 20

=cut

__PACKAGE__->add_columns(
  "dog_id",
  { data_type => "integer auto_increment", is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 60 },
  "dob",
  { data_type => "date", is_nullable => 1 },
  "dog_type",
  { data_type => "varchar", is_nullable => 1, size => 60 },
  "dog_pass",
  { data_type => "varchar", is_nullable => 1, size => 20 },
);

=head1 PRIMARY KEY

=over 4

=item * L</dog_id>

=back

=cut

__PACKAGE__->set_primary_key("dog_id");

=head1 RELATIONS

=head2 logs

Type: has_many

Related object: L<DogLogDB::Schema::Result::Log>

=cut

__PACKAGE__->has_many(
  "logs",
  "DogLogDB::Schema::Result::Log",
  { "foreign.dog_id" => "self.dog_id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07042 @ 2014-12-27 19:42:42
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ruDsSP10vprY4skoSHK7DQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
