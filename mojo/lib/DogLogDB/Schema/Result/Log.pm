use utf8;
package DogLogDB::Schema::Result::Log;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

DogLogDB::Schema::Result::Log

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<log>

=cut

__PACKAGE__->table("log");

=head1 ACCESSORS

=head2 log_id

  data_type: 'integer auto_increment'
  is_nullable: 0

=head2 dog_id

  data_type: 'int unsigned'
  is_foreign_key: 1
  is_nullable: 1

=head2 dog_did

  data_type: 'varchar'
  is_nullable: 1
  size: 7

=head2 outside

  data_type: 'boolean'
  default_value: true
  is_nullable: 1

=head2 amount

  data_type: 'float'
  default_value: 0
  is_nullable: 1

=head2 log_when

  data_type: 'datetime'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "log_id",
  { data_type => "integer auto_increment", is_nullable => 0 },
  "dog_id",
  { data_type => "int unsigned", is_foreign_key => 1, is_nullable => 1 },
  "dog_did",
  { data_type => "varchar", is_nullable => 1, size => 7 },
  "outside",
  { data_type => "boolean", default_value => \"true", is_nullable => 1 },
  "amount",
  { data_type => "float", default_value => 0, is_nullable => 1 },
  "log_when",
  { data_type => "datetime", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</log_id>

=back

=cut

__PACKAGE__->set_primary_key("log_id");

=head1 RELATIONS

=head2 dog

Type: belongs_to

Related object: L<DogLogDB::Schema::Result::Dog>

=cut

__PACKAGE__->belongs_to(
  "dog",
  "DogLogDB::Schema::Result::Dog",
  { dog_id => "dog_id" },
  {
    is_deferrable => 0,
    join_type     => "LEFT",
    on_delete     => "NO ACTION",
    on_update     => "NO ACTION",
  },
);


# Created by DBIx::Class::Schema::Loader v0.07042 @ 2014-12-27 19:42:42
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:a2OfJxbgfyworZtziVa3gQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
