#!/usr/bin/env perl
use strict; use warnings; 
#use 5.20.1;
use feature 'say';

use Data::Dumper;
use Mojolicious::Lite;
use JSON;
use lib './lib';
use DogLogDB::Schema;
use DateTime;

# allow cors
#use Mojo::Base 'Mojolicious';
#plugin 'Mojolicious::Plugin::CORS';

use FindBin;
our $dbfile="$FindBin::Bin/db/doglog.db";
my $db = DogLogDB::Schema->connect("dbi:SQLite:dbname=$dbfile");



sub func {
 my $c=shift;
 $c->render(json=>{res=>'yo'});
}

sub listlog {
 say "listlog!";
 my $c=shift;
 my $dog=$c->param('dog_id');
 my $logs=$db->resultset('Dog')->find($dog)->logs();
 $logs->result_class('DBIx::Class::ResultClass::HashRefInflator');
 my $data;
 push @$data, $_ while ($_=$logs->next); 
 $c->render(json=>$data);
}

sub addlog {
 say "addlog!";
 my $c=shift;
 # from json
 my $req=$c->req->body || '{}';
 my $json=decode_json($req);
 say "json: ";
 say Dumper($json);

 # from param
 my $dog=$c->param('dog_id');
 my $logs=$db->resultset('Dog')->search($dog);

 # to add
 my @params=qw/dog_did outside amount log_when/;

 $c->render(json=>{error=>'no data'}) and return unless $json->{data};

 #my %toadd = map  {$_=>$json->{data}->{$_} if $json->{data}->{$_} } @params;
 #$c->render(json=>{error=>'no inputs'}) and return unless scalar(grep {$_} values %toadd);
 
 my %toadd;
 $toadd{dog_id} = $dog;
 for my $p ( @params ){
   $toadd{$p} = $json->{data}->{$p} if $json->{data}->{$p};
 }

 # time
 if( defined $json->{data}->{when} ) {
   $toadd{log_when}= DateTime->now();
 }


 say Dumper(%toadd);

 my $add = $db->resultset('Log')->create(\%toadd);
 $add=$add->insert();
 $c->render(json=>{id=>$add->log_id});

}


### ROUTES
get '/' => sub { my $c=shift; $c->render(text=>"blah"); };
## logs
get    'api/log/:dog_id/'    => \&listlog;
post   'api/log/:dog_id/'    => \&addlog;
get    'api/log/:dog_id/:log_id' => \&func;
put    'api/log/:dog_id/:log_id' => \&func;
#delete 'api/person/:id'->to('person#delete');
#

## Dogs
get    'api/dog'     => \&func;
post   'api/dog'     => \&func;
get    'api/dog/:dog_id' => \&func;
put    'api/dog/:dog_id' => \&func;


options '*' => sub {
  say "option *!";
  my $self = shift;
  $self->res->headers->header('Access-Control-Allow-Origin'=> '*');
  $self->res->headers->header('Access-Control-Allow-Credentials' => 'true');
  $self->res->headers->header('Access-Control-Allow-Methods' => 'GET, OPTIONS, POST, DELETE, PUT');
  $self->res->headers->header('Access-Control-Allow-Headers' => 'Content-Type, X-CSRF-Token');
  $self->res->headers->header('Access-Control-Max-Age' => '1728000');
  $self->respond_to(any => { data => '', status => 200 });
};

hook after_dispatch => sub {
  my $self=shift;
 $self->res->headers->header('Access-Control-Allow-Origin'=> '*');
};

app->start;
