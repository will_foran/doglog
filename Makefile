init: 
	#cordova create doglog
	#cd doglog 
	cordova platform add android
	cordova plugin add org.apache.cordova.geolocation
	cd www
	bower install

db:
	cd mojo
	[ -r db ] || mkdir db
	# parsediasql from Parse::Dia::SQL http://search.cpan.org/~aff/Parse-Dia-SQL-0.23/lib/Parse/Dia/SQL.pm
	parsediasql --file doglog.dia --db sqlite3fk > db/doglog.sqlite
	sed -i 's/int unsigned not null/INTEGER PRIMARY KEY/g' db/doglog.sqlite
	sqlite3 db/doglog.db < db/doglog.sqlite
	echo "insert into dog (name,dob,dog_type,dog_pass)  values ('Maslow','2014/10/27','labradoodle','pass');" | sqlite3 db/doglog.db
	echo "insert into log (dog_id,dog_did,outside,amount,log_when)  values (1,'test',1,0, date('now') );" | sqlite3 db/doglog.db
	#
	# also make a mysql version
	#parsediasql --file doglog.dia --db mysql-innodb > db/doglog.mysql
	dbicdump -o dump_directory=./lib DogLogDB::Schema dbi:SQLite:dbname=db/doglog.db
